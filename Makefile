# Makefile for Make

CXX = dpcpp
CXXFLAGS = -O2 -g -std=c++17

BUFFER_EXE_NAME = matrix_spars_dense
BUFFER_SOURCES = src/main.cpp

MKL_COPTS = -DMKL_ILP64  -I"${MKLROOT}/include"
MKL_LIBS = -L${MKLROOT}/lib/intel64 -lmkl_sycl -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lsycl -lOpenCL -lpthread -lm -ldl

DPCPP_OPTS = $(MKL_COPTS) -fsycl-device-code-split=per_kernel $(MKL_LIBS)

all: build_buffers

build_buffers:
	$(CXX) $(CXXFLAGS) -o $(BUFFER_EXE_NAME) $(BUFFER_SOURCES) $(DPCPP_OPTS)

run: $(BUFFER_EXE_NAME)
	./$(BUFFER_EXE_NAME)

clean: 
	rm -f $(BUFFER_EXE_NAME) $(USM_EXE_NAME)

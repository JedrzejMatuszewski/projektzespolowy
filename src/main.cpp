#include <iostream>
#include <limits>

#include <CL/sycl.hpp>
#include "oneapi/mkl.hpp"

void fill_matrices(double* matrix_a, double* matrix_b, int matrix_size_m, int matrix_size_k, int matrix_size_n, int leading_dimension_a, int leading_dimension_b);
bool compare_results();

class TimeCounter {
public:
    TimeCounter() {}
};

int main()
{
    try {
        // GEMM operation is:
        //
        //      C = alpha * op(A) * op(B) + beta * C
        //
        // In our case GEMM will calculate C = A * B
        //
        // Matrices are stored in row-major layout.

        auto transA = oneapi::mkl::transpose::nontrans;
        auto transB = oneapi::mkl::transpose::nontrans;

        // Matrix data sizes.
        // 
        // A is m x k
        // B is k x n  
        // --> C is m x n
        int m = 600;
        int k = 1200;
        int n = 2400;

        // Leading dimensions of data. For row-major matrices, the leading
        // dimension is the stride between adjacent rows.
        int lda = k;
        int ldb = n;
        int ldc = n;

        // Scaling factors.
        double alpha = 1.0;
        double beta = 0.0;

        // Create a queue on the default device.
        sycl::queue device_queue{ sycl::default_selector{} };

        std::cout << "Device: "
            << device_queue.get_device().get_info<sycl::info::device::name>()
            << std::endl;

        // Allocate shared memory for matrices.
        auto A = sycl::malloc_shared<double>(m * k, device_queue);
        auto B = sycl::malloc_shared<double>(k * n, device_queue);
        auto C = sycl::malloc_shared<double>(m * n, device_queue);

        if (!A || !B || !C) {
            std::cerr << "Could not allocate memory for matrices." << std::endl;
            exit(1);
        }

        // Initialize matrix data.
        fill_matrices(A, B, m, k, n, lda, ldb);

        std::cout << "Matrices: "
            << " A (" << m << 'x' << k << ") *"
            << " B (" << k << 'x' << n << ")  --> "
            << " C (" << m << 'x' << n << ")\n";

        // Call GEMM to do matrix multiplication, asynchronously.
        std::cerr << "Launching oneMKL GEMM calculation - kernel dense..." << std::endl;

        oneapi::mkl::blas::row_major::gemm(device_queue, transA, transB, m, n, k,
            alpha, A, lda, B, ldb, beta, C, ldc);

        // Wait for oneMKL computation to complete.
        device_queue.wait_and_throw();
        
        // Check results for accuracy.
        bool ok = compare_results();

        // Free memory.
        free(A, device_queue);
        free(B, device_queue);
        free(C, device_queue);

        if (!ok)
            exit(2);
    }
    catch (const std::exception& e) {
        std::cerr << "An exception occurred: "
            << e.what() << std::endl;
        exit(1);
    }
}

void fill_matrices(double* matrix_a, double* matrix_b, int matrix_size_m, int matrix_size_k, int matrix_size_n, int leading_dimension_a, int leading_dimension_b)
{
    // Initialize random matrix data
    // 
    // This is right now just for development purpose

    for (int i = 0; i < matrix_size_m; i++)
        for (int j = 0; j < matrix_size_k; j++)
            matrix_a[i * leading_dimension_a + j] = double(rand()) / RAND_MAX;

    for (int i = 0; i < matrix_size_k; i++)
        for (int j = 0; j < matrix_size_n; j++)
            matrix_b[i * leading_dimension_b + j] = double(rand()) / RAND_MAX;
}

bool compare_results()
{
    bool ok = true;

    // Compare results here..

    if (ok)
        std::cout << "Results are accurate.\n";

    return ok;
}

